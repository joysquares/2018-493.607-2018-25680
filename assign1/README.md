# Assign1
This repository contains assignment 1 of Core Software(493.607) class.

## For What
Given a text file, this application parses words from whole sentences.

Then, checks the frequency of each word, finally it outputs pairs of word and its frequency in descending order of frequency.

## Conditions
The conditions for parsing are the followings:

- Read a text file as an argument such like `assign1 Aladdin.txt`.
- Tokenize the text file such that:
    - Divide a sentence into words
    - Each word does not include following symbols:
    - ```
        \t (tab), \r, \n, ' ' (space), '"', ':', ',', ';', '`', ‘(‘, ‘)’, ‘!’, ‘?’, ‘{‘, ‘}’, ‘.’, ‘\’’
      ```
    - You should keep `'`, `.` in the middle of a word
    - Change all upper-case letters to lower-case ones
    
- Count the number of occurrence of each distinguished word
- Sort the word by their frequency (# of occurrence) in descending order
- Print (into standard output) the pair of word and its frequency in the following way (one pair per line):
```
the: 10000
a: 9900
...
```

## Undefined
Above conditions are very clear except bellow things:

- There are so many cases of words include `-`, `--` or `---`:
    - `hey--it's`
    - `closer--(Camera`
    - `maker--also`
    - `pocket)---`
    - `son-of-a-jackal`
    - `beat-up`
    - `Ten-thousand`
    - `ix-nay`
    - `papa-in-law`
    - `part-and-parcel`
    - `dive-bomb`
    - `...`
      
- E-mail address:
    - `<34RQNPQ@CMUVM.CSV.CMICH.EDU>`
      
- There are several words include `...`:
    - `choose...I`
    - `consider...this.`
    - `You...are`
    - `this...diamond`
    - `...`
      
- Spacing word after end of sentence would be left out:
    - `There.Welcome` 
    - `mouth.The`
    - `on.ALADDIN`

- Include `/` middle of the word: 
    - `/Where` 
    - `/And`
    - `b/w`
    - `tailor/fashion` 
   
## Exception

- Handle all sequences of `-` as a token
    - `hey--it's` -> `hey` and `it's`
    - `closer--(Camera` -> `closer` and `camera`
    - `son-of-a-jackal` -> `son`, `of`, `a` and `jackal`
    - `...`
    
- Handle `...` as a token
    - `choose...I` -> `choose` and `i`
    - `consider...this.` -> `consider` and `this`
    - `...`

- Add logic for e-mail address
    - `[A-Za-z0-9\\<]+([\\.]?[\\']?[\\-]?[\\@]?[\\/]?[A-Za-z0-9\\>]+)*`

- Split each case into 2 words considering spacing word
    - `There.Welcome` -> `there` and `welcome`
    - `mouth.The` -> `mouth` and `the`
    - `on.ALADDIN` -> `on` and `aladdin`
    - The rule to check whether given word consists of two words or not:
        - `[a-z]\\.[A-Z]`
        - lower-case + '.' + upper-case means that the given word consists of two words and spacing word were left out, so should be parsed

- Handle only middle cases of `/` as a word
    - `/Where` -> `where`
    - `b/w` doesn't change
    - `tailor/fashion` doesn't change
    
## Makefile
Before running app, you must build and compile using `Makefile`:
```sh
➜ make all
```
or shortly just:
```sh
➜ make
```
then, `Makefile` automatically makes objective `.o` file from `.cpp` file using `gnu c++ compiler` with version `std=gnu++14`.
```sh
➜ make
g++ -g   -std=gnu++14 -c main.cpp
g++ -g   -std=gnu++14 -o assign1 main.o
```
After these processing, you can see the `assign1` file to run this app.

If you want to remove all stuff as a result of compilation, 
```sh
➜ make clean
```
then, `assign1` and `main.o` will be removed with this message:
```sh
➜ make clean
rm -f assign1 main.o
```

## Run
After compilation, you must run the app with argument `Aladdin.txt`:
```sh
➜ ./assign1 Aladdin.txt
```
## Result

```sh
the: 869
a: 521
to: 448
and: 433
aladdin: 383
you: 277
of: 254
he: 247
i: 220
jafar: 203
jasmine: 194
it: 188
his: 186
in: 180
is: 170
abu: 168
genie: 166
  .
  .
  .
acting: 1
acrobatic: 1
acrobat: 1
accent: 1
absorbed: 1
absolute: 1
absentmindedly: 1
abracadabra: 1
able: 1
aaaaahhhhh: 1
a'la: 1
57: 1
46: 1
40: 1
4: 1
3: 1
1992: 1
```