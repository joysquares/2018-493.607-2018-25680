#include <iostream>
#include <iterator>
#include <string>
#include <regex>
#include <fstream>
#include <map>

void save_to_buffer(std::string &src, std::map<std::string, int> &dst)
{
    /** convert uppercase to lowercase **/
    std::transform(src.begin(), src.end(), src.begin(),
                   [](auto c) -> auto { return std::tolower(c); });
    /** save a word to the buffer **/
    dst[src] += 1;
}

template <typename A, typename B>
std::pair<B, A> flip_pair(const std::pair<A, B> &p)
{
    return std::pair<B, A>(p.second, p.first);
}

template <typename A, typename B>
std::multimap<B, A> flip_map(const std::map<A, B> &src)
{
    std::multimap<B, A> dst;
    std::transform(src.begin(), src.end(), std::inserter(dst, dst.begin()), flip_pair<A, B>);
    return dst;
}

void print_word(std::multimap<int, std::string> &m, bool reverse) {
    if (reverse) {
        for (auto i = m.rbegin(); i != m.rend(); ++i)
            std::cout << i->second << ": " << i->first << '\n';
    } else {
        for (auto i : m)
            std::cout << i.second << ": " << i.first << '\n';
    }
}

int main(int argc, char** argv) {
    /** declaration for a buffer(map) to save pair of word and freq **/
    std::map<std::string, int> m;

    if (argc == 2) {
        /** open file **/
        std::ifstream openFile(argv[1]);

        /** success **/
        if (openFile.is_open()) {
        std::string line;
        /** read a line from the file until the ptr reaches EOF **/
            while (getline(openFile, line)) {
                /** regular expression for parsing **/
                std::regex tok_re("[A-Za-z0-9\\<]+([\\.]?[\\']?[\\-]?[\\@]?[\\/]?[A-Za-z0-9\\>]+)*");
                auto words_begin =
                      std::sregex_iterator(line.begin(), line.end(), tok_re);
                auto words_end = std::sregex_iterator();
                /** parse a line **/
                for (auto i = words_begin; i != words_end; ++i) {
                    auto match = *i;
                    auto match_str = match.str();
                    /** check a word includes '.' character **/
                    auto n = match_str.find('.');
                    /** if has **/
                    if (match_str.find('.') != std::string::npos) {
                        /** check the condition : [a-z].[A-Z] **/
                        /** if ture, consider spacing word would be left out **/
                        if ((match_str[n-1] >= 'a' && match_str[n-1] <= 'z')
                        && (match_str[n+1] >= 'A' && match_str[n+1] <= 'Z')) {
                            /** split a word into 2 words **/
                            auto preStr = match_str.substr(0, n);
                            auto postStr = match_str.substr(n + 1);
                            /** save words to map **/
                            save_to_buffer(preStr, m);
                            save_to_buffer(postStr, m);
                        }
                    }
                    /** doens't include '.' **/
                    else {
                      /** save words to map **/
                      save_to_buffer(match_str, m);
                    }
                }
            }
            /** close the file buffer **/
            openFile.close();
            /** for using map's sorting algorithm, switch keys and values **/
            std::multimap<int, std::string> dst = flip_map(m);
            /** print the words in descending order **/
            print_word(dst, true);
        }
        else{
            std::cout << "Wrong argument. Please retry.\n";
        }
    }
    else {
      std::cout << "Wrong argument. Please retry.\n";
    }
    return 0;
}
