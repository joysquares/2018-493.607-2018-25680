import matplotlib.pyplot as plt
import numpy as np
import re
from collections import OrderedDict


file_name = 'Aladdin.txt'

p = re.compile("\w+[\\.\\@\\'\\/\w+]*")

buffer = {}

with open(file_name, 'r') as f:
    for i in f.readlines():
        m = p.findall(i)
        for j in m:
            while j[-1] in ['.', "'"]:
                j = j[:-1]

            if '...' in j:
                buffer.update({k.lower() : buffer.get(k.lower(), 0) + 1 for k in j.split('...')})

            elif '.' in j:
                if 'a' <= j[j.find('.') - 1] <= 'z':
                    buffer.update({k.lower() : buffer.get(k.lower(), 0) + 1 for k in j.split('.')})

            else:
                buffer[j.lower()] = buffer.get(j.lower(), 0) + 1

temp = sorted(sorted(OrderedDict(buffer).items(), key=lambda x : x[0], reverse=True), key=lambda x: x[1], reverse=True)

x, y = [], []

for i in temp[0:20]:
    x.append(i[0])
    y.append(i[1])

y = np.array(y) / np.sum(y)

fig = plt.figure(figsize=(10, 5))
ax = plt.subplot()
p1 = ax.plot(x, y, color='tab:red', label='PDF')
ax.tick_params(axis='y', labelcolor='tab:red')
for i in ax.xaxis.get_ticklabels():
    i.set_rotation(300)
plt.title('Graph')
ax.set_xlabel('Word')
ax.set_ylabel('PDF', color='tab:red')

ax2 = ax.twinx()
p2 = ax2.plot(x, np.cumsum(y), color='tab:blue', label='CDF')
ax2.set_ylabel('CDF', color='tab:blue')
ax2.tick_params(axis='y', labelcolor='tab:blue')
lns = p1 + p2
labs = [l.get_label() for l in lns]
ax.legend(lns, labs, loc='right')
plt.show()