# Assign3
This repository contains assignment 3 of Core Software(493.607) class.

## For What
Draw the CDF and PDF of the frequency of words that were obtained from [Assignment 2](../assign2)(sorted in descending order by the frequency) with the following directions:

- Use only 20 most frequently used words.
- Display chart, horizontal / vertical axis titles
- Display legends

## Run
```sh
➜ python assign3.py
```

## Result
Two plots in a graph, red and blue are PDF and CDF, respectively.

![result](result_img.png)
