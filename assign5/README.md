# Assign5
This repository contains assignment 5 of Core software(493.607) class.

## How to make a PDF
1. Open `assign5.tex` file using tex editor (e.g, [TexShop](https://pages.uoregon.edu/koch/texshop/))
2. Set file as `BibTex` and press `Typeset`
3. Set file as `LaTex` and press `Typeset`
4. Press `Typeset` again
