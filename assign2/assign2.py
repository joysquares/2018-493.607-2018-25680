import sys
import re
from collections import Counter, OrderedDict


if len(sys.argv) == 2:
    
    file_name = sys.argv[1]

    p = re.compile("\w+[\\.\\@\\'\\/\w+]*")
    
    buffer = {}
  
    try:
        with open(file_name, 'r') as f:
            for i in f.readlines():
                m = p.findall(i)
                for j in m:
                    while j[-1] in ['.', "'"]:
                        j = j[:-1]

                    if '...' in j:
                        buffer.update({k.lower() : buffer.get(k.lower(), 0) + 1 for k in j.split('...')})

                    elif '.' in j:
                        if 'a' <= j[j.find('.') - 1] <= 'z':
                            buffer.update({k.lower() : buffer.get(k.lower(), 0) + 1 for k in j.split('.')})

                    else:
                        buffer[j.lower()] = buffer.get(j.lower(), 0) + 1

        temp = sorted(OrderedDict(buffer).items(), key=lambda x : x[0], reverse=True)
        for i in sorted(temp, key=lambda x : x[1], reverse=True):
            print("{} : {}".format(i[0], i[1]))
            
    except:
        print("Wrong input!!\n Please try again...")

else:
    print("Wrong input!!\n Please try again...")